
def validate(password: str) -> bool:
    """
    Jelszo validalo

    Egy jelszo akkor valid, ha a kovetkezo kovetelmenyeknek megfelel:
    - 8 es 20 karakter kozotti a hossza (inkluziv)
    - Legalabb egy szamot tartalmaz
    - Legalabb egy kisbetut tartalmaz
    - Legalabb egy nagybetut tartalmaz
    - Legalabb egyet tartalmaz a kovetkezo a karakterekbol: !"§$%&/()=?

    Parameterek:
        password (str): A validalando jelszo

    Returns:
        bool: True ha a jelszo valid, egyebkent False 
    """
    
    # 8 es 20 karakter kozotti hosszusagu (inkluziv)
    length_ok = len(password) in range(8, 21)

    # Legalabb egy szamot tartalmaz
    has_number = any(c.isdigit() for c in password)

    # Legalabb egy kisbetut tartalmaz
    has_lower = any(c.islower() for c in password)

    # Legalabb egy nagybetut tartalmaz
    has_upper = any(c.isupper() for c in password)

    # Legalabb egyet tartalmaz a kovetkezo a karakterekbol: !"§$%&/()=?
    has_special = any(c in '!"§$%&/()=?' for c in password)

    return length_ok and has_number and has_lower and has_upper and has_special


if __name__ == "__main__":
    print(validate.__doc__)
    print('Kerem adjon meg egy jelszot:')
    password_input = input()

    ##ternaris operator
    # 2 elemu tuple, 0-as index False, 1-es index True es a validalas
    print(('A jelszo nem felel meg', 'A jelszo megfelel') [validate(password_input)])
